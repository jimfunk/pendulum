# Installation

    python3.5 setup.py install

# Usage

    pendulum --help
        usage: pendulum [-h] [--review] [--dates DATES]

        Post time from toggl to JIRA and Freshdesk

        optional arguments:
          -h, --help     show this help message and exit
          --review       Review entries without posting
          --dates DATES  Dates to post entries from in YYYY-MM-DD, comma-separated

# Config file
The config file should be created at /etc/pendulum.conf and follow this configuration.

    [toggl]
    api_token = ASDFJL@J#$LKNAIONSDFPIONPO#N
    workspace_id = 12345

    [jira]
    server = https://jira.example.com
    username = foo.bar
    password = hunter2

    [freshdesk]
    domain = mysupport.freshdesk.com
    token = ASDFLJOI89DS23
    agent_id = 234235262

Follow [this guide](https://support.freshdesk.com/support/solutions/articles/197883-finding-user-id-responder-id-of) to get your agent_id from freshdesk.

# Development

    pip install -r requirements.pip
    polytester --autoreload
