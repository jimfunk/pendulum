from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name='pendulum',
    version='0.1',
    description='Posts time from Toggl to Jira and Freshdesk',
    long_description=readme(),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.5',
    ],
    keywords='pendulum jira toggl',
    url='https://gitlab.com/bcdynamic/pendulum',
    author='Brandon Cazander',
    author_email='brandon.cazander@multapplied.net',
    license='MIT',
    packages=['pendulum', 'pendulum.lib'],
    install_requires=[
        'markdown',
        'colorama==0.3.7',
        'jira==1.0.7',
        'python-dateutil==2.6.0',
        'pytz==2016.7',
        'requests==2.12.1',
        'slackclient>=1.0.9',
    ],
    test_suite='nose.collector',
    tests_require=['nose'],
    entry_points={
        'console_scripts': ['pendulum=pendulum.cli:main'],
    },
    include_package_data=True,
    zip_safe=False
)
