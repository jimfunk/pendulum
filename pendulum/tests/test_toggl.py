import unittest

import dateutil.parser

from pendulum.lib.toggl import TimeEntry

TOGGLE_RESPONSE = {
    "id": 505224181,
    "wid": 1767466,
    "billable": False,
    "start": "2016-12-27T21:30:50+00:00",
    "stop": "2016-12-27T21:47:31+00:00",
    "duration": 1001,
    "description": "ADM-11 [8505] Testing entry",
    "tags": [],
    "duronly": False,
    "at": "2016-12-27T21:47:55+00:00",
    "uid": 1080678
}


class TimeEntryTest(unittest.TestCase):
    def setUp(self):
        self.entry = TimeEntry(**TOGGLE_RESPONSE)

    def test_posted_to_jira(self):
        self.assertFalse(self.entry.posted_jira)
        self.entry.tags = ['0-Posted']
        self.assertTrue(self.entry.posted_jira)

    def test_issue(self):
        self.assertEqual(self.entry.issue, 'ADM-11')
        self.entry.description = 'BI-4423 Foo bar'
        self.assertEqual(self.entry.issue, 'BI-4423')

    def test_ticket_number(self):
        self.assertEqual(self.entry.ticket_number, '8505')

    def test_ticket_number_none(self):
        self.entry.description = 'ADM-15 Foo bar'
        self.assertEqual(self.entry.ticket_number, None)

    def test_started(self):
        date = dateutil.parser.parse(self.entry.start)
        self.assertEqual(self.entry.started, date)

    def test_postable(self):
        self.assertTrue(self.entry.postable)
        self.entry.duration = -1
        self.assertFalse(self.entry.postable)

    def test_duration_rounding(self):
        self.entry.duration = 29
        self.assertEqual(self.entry.time_in_seconds, 60)
        self.entry.duration = 61
        self.assertEqual(self.entry.time_in_seconds, 60)
        self.entry.duration = 1001
        self.assertEqual(self.entry.time_in_seconds, 1020)

    def test_running(self):
        self.assertFalse(self.entry.running)
        self.entry.duration = -1
        self.assertTrue(self.entry.running)
        self.entry.duration = -3423423
        self.assertTrue(self.entry.running)

    def test_freshdesk_time(self):
        self.assertEqual(self.entry.freshdesk_time, '00:17')
        self.entry.duration = 8201
        self.assertEqual(self.entry.freshdesk_time, '02:17')

    def test_mark_posted(self):
        self.entry.mark_jira_posted()
        self.assertEqual(self.entry.tags, ['0-Posted'])
        self.entry.mark_freshdesk_posted()
        self.assertEqual(self.entry.tags, ['0-Posted', '1-Freshdesk'])
        self.entry.mark_freshdesk_posted()
        self.assertEqual(self.entry.tags, ['0-Posted', '1-Freshdesk'])

    def test_repr(self):
        self.assertEqual(repr(self.entry), 'ADM-11 [8505] Testing entry')

    def test_for_freshdesk(self):
        self.assertTrue(self.entry.for_freshdesk)
        self.entry.description = 'ADM-15 Foo bar'
        self.assertFalse(self.entry.for_freshdesk)

    def test_needs_posting_jira_only(self):
        self.entry.description = 'ADM-15 Foo bar'
        self.assertTrue(self.entry.needs_posting)
        self.entry.tags = ['0-Posted']
        self.assertFalse(self.entry.needs_posting)

    def test_needs_posting_freshdesk(self):
        self.assertTrue(self.entry.needs_posting)
        self.entry.tags = ['0-Posted']
        self.assertTrue(self.entry.needs_posting)
        self.entry.tags = ['1-Freshdesk']
        self.assertTrue(self.entry.needs_posting)
        self.entry.tags = ['0-Posted', '1-Freshdesk']
        self.assertFalse(self.entry.needs_posting)

    def test_billable_freshdesk(self):
        self.assertFalse(self.entry.is_billable)
        self.entry.tags = ['2-Billable']
        self.assertTrue(self.entry.is_billable)

    def test_hours(self):
        self.entry.duration = 6960
        self.assertEqual(self.entry.hours, '01')
        self.assertEqual(self.entry.minutes, '56')

    def test_is_support(self):
        self.assertTrue(self.entry.is_support)
        self.entry.description = 'BI-1234 foo bar'
        self.assertFalse(self.entry.is_support)

    def test_is_admin(self):
        self.assertTrue(self.entry.is_admin)
        self.entry.description = 'BI-1234 foo bar'
        self.assertFalse(self.entry.is_admin)

    def test_jira_params(self):
        self.assertEqual(
            self.entry.jira,
            {
                'issue': 'ADM-11',
                'started': self.entry.started,
                'timeSpentSeconds': 1020,
                'comment': 'Testing entry',
            }
        )

    def test_freshdesk_params(self):
        self.assertEqual(
            self.entry.freshdesk,
            {
                'ticket_id': '8505',
                'start_time': self.entry.start,
                'note': 'Testing entry',
                'time_spent': '00:17',
                'billable': False,
            }
        )
