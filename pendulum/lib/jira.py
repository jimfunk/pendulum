from jira import JIRA


class Jira:
    def __init__(self, server, username, password):
        self.server = server
        self.username = username
        self.password = password
        self._jira = JIRA(self.server, basic_auth=(self.username, self.password))

    def get_issue(self, id):
        return self._jira.issue(id)

    def post(self, **kwargs):
        return self._jira.add_worklog(**kwargs).raw
